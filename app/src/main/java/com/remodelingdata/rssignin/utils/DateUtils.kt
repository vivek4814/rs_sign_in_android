package com.remodelingdata.rssignin.utils

import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.zip.DataFormatException

class DateUtils {
    companion object
    {
        fun getDate(milliSeconds: Long, dateFormat: String?): String? {
            // Create a DateFormatter object for displaying date in specified format.
            val formatter = SimpleDateFormat(dateFormat)

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            val calendar: Calendar = Calendar.getInstance()
            calendar.setTimeInMillis(milliSeconds)
            return formatter.format(calendar.getTime())
        }

        fun getServerDate(dateTime: Long, dateFormat: String?): String?
        {
            var formattedDate = ""
            val formatter = SimpleDateFormat(dateFormat)

            val calendar: Calendar = Calendar.getInstance()
            calendar.setTimeInMillis(dateTime)
            formattedDate = formatter.format(calendar.time)
            return formattedDate
        }

        fun getFormattedDateTime(dateTime: Long, dateFormat: String?): String? {
            // Create a DateFormatter object for displaying date in specified format.
            var formattedDate = ""
            val formatter = SimpleDateFormat(dateFormat)
            //2021-02-24T00:25:25.000Z
//            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val calendar: Calendar = Calendar.getInstance()
            calendar.setTimeInMillis(dateTime)
            formattedDate = formatter.format(calendar.time)

            return formattedDate
        }
    }
}