package com.remodelingdata.rssignin.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog

class AlertDialogUtils {

    companion object
    {
        fun showAlertDialog(from: Context, title: String, msg: String, positiveBtnText: String)
        {
            AlertDialog.Builder(from)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(positiveBtnText, { dialog, _ -> dialog.dismiss() })
                .show()
        }
    }
}