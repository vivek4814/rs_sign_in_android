package com.remodelingdata.rssignin.utils

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetDialog
import pl.aprilapps.easyphotopicker.*
import java.io.File
import com.remodelingdata.rssignin.R

class ImageSelectionHelper(context: Activity, listener: ImageSelectionListener): View.OnClickListener {

    private var mContext: Activity? = null
    private var imageSelectionListener: ImageSelectionListener? = null
    private var mBottomSheetDialog: BottomSheetDialog? = null

    private var easyImage: EasyImage? = null
    private val LEGACY_WRITE_PERMISSIONS = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private val LEGACY_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 456
    private val CHOOSER_PERMISSIONS_REQUEST_CODE = 7459
    private val GALLERY_REQUEST_CODE = 7502

    init {
        this.mContext = context
        this.imageSelectionListener = listener
        easyImage = EasyImage.Builder(mContext!!)
                .setChooserTitle("Choose image")
                .setCopyImagesToPublicGalleryFolder(true) // THIS requires granting WRITE_EXTERNAL_STORAGE permission for devices running Android 9 or lower
                .setChooserType(ChooserType.CAMERA_AND_GALLERY)
                .setFolderName(mContext?.resources?.getString(R.string.app_name)!!)
                .allowMultiple(false) //.setStateHandler(this)
                .build()
    }

    fun openImageSelectionDialog() {
        mBottomSheetDialog = BottomSheetDialog(mContext!!, R.style.CustomBottomSheetDialogTheme)
        val sheetView: View = mContext!!.layoutInflater.inflate(R.layout.dialog_choose_photo, null)
        mBottomSheetDialog?.setContentView(sheetView)
        mBottomSheetDialog?.show()
        val tv_fromGallery = sheetView.findViewById<TextView>(R.id.tv_gallery)
        val tv_fromCamera = sheetView.findViewById<TextView>(R.id.tv_camera)
        val tv_cancelDialog: CardView = sheetView.findViewById(R.id.lyt_dismissDialog)
        tv_fromGallery.setOnClickListener(this)
        tv_fromCamera.setOnClickListener(this)
        tv_cancelDialog.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.tv_gallery -> {
                mBottomSheetDialog?.dismiss()
                if (isLegacyExternalStoragePermissionRequired()) {
                    requestLegacyWriteExternalStoragePermission()
                } else {
                    easyImage?.openGallery(mContext!!)
                }
            }
            R.id.tv_camera -> {
                mBottomSheetDialog?.dismiss()
                val PERMISSIONS = arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                if (!hasPermissions(*PERMISSIONS)) {
                    ActivityCompat.requestPermissions(
                        mContext!!,
                        PERMISSIONS,
                        CHOOSER_PERMISSIONS_REQUEST_CODE
                    )
                } else {
                    if (isDeviceSupportCamera()) {
                        easyImage?.openCameraForImage(mContext!!)
                    } else {
                        Toast.makeText(
                            mContext,
                            "Your device does not support camera",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            R.id.lyt_dismissDialog -> {
                mBottomSheetDialog?.dismiss()
            }
        }
    }

    private fun isLegacyExternalStoragePermissionRequired(): Boolean {
        val permissionGranted = ContextCompat.checkSelfPermission(
            mContext!!,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
        return Build.VERSION.SDK_INT < 29 && !permissionGranted
    }

    private fun requestLegacyWriteExternalStoragePermission() {
        ActivityCompat.requestPermissions(
            mContext!!,
            LEGACY_WRITE_PERMISSIONS,
            LEGACY_EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE
        )
    }


    /**
     * Check Camera Availability
     *
     * @return
     */
    fun isDeviceSupportCamera(): Boolean {
        // this device has a camera
        return mContext!!.packageManager.hasSystemFeature(
            PackageManager.FEATURE_CAMERA
        )
    }

    private fun hasPermissions(vararg permissions: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(mContext!!, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == CHOOSER_PERMISSIONS_REQUEST_CODE && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            easyImage?.openChooser(mContext!!)
        } else if (requestCode == GALLERY_REQUEST_CODE && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            easyImage?.openGallery(mContext!!)
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        easyImage?.handleActivityResult(
            requestCode,
            resultCode,
            data,
            mContext!!,
            object : DefaultCallback() {
                override fun onMediaFilesPicked(imageFiles: Array<MediaFile>, source: MediaSource) {
                    for (i in 0 until imageFiles.size) {
                        val imageFile = imageFiles.get(i)
                        imageSelectionListener!!.capturedImage(null, imageFile.file)
                    }
                }

                override fun onImagePickerError(error: Throwable, source: MediaSource) {
                    //Some error handling
                    error.printStackTrace()
                }

                override fun onCanceled(source: MediaSource) {
                    //Not necessary to remove any files manually anymore
                }
            })
    }

    /**
     * Listener interface for receiving callbacks from the CaptureImageUtils.
     */
    interface ImageSelectionListener {
        fun capturedImage(uri: Uri?, imageFile: File?)
    }
}