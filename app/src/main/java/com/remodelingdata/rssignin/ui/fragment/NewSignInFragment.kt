 package com.remodelingdata.rssignin.ui.fragment

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.google.type.DateTime
import com.remodelingdata.rssignin.R
import com.remodelingdata.rssignin.constants.AppConstant
import com.remodelingdata.rssignin.model.*
import com.remodelingdata.rssignin.ui.fragment.adapter.*
import com.remodelingdata.rssignin.utils.*
import com.ve.countertop.preferences.Prefs
import kotlinx.android.synthetic.main.fragment_new_sign_in.view.*
import kotlinx.android.synthetic.main.toolbar_main.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class NewSignInFragment : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener,
    ImageSelectionHelper.ImageSelectionListener {

    private var mView: View? = null
    private var mAuth: FirebaseAuth? = null
    val db = Firebase.firestore
    private var mImageFile: File? = null
    private var mImageCaptureHelper: ImageSelectionHelper? = null

    private var mShowRoomList: ArrayList<ShowRoomDataModel>? = null
    private var mShowRoomListAdapter: ShowRoomListAdapter? = null

    private var mSpokeWithList: ArrayList<SpokeWithDataModel>? = null
    private var mSpokeWithListAdapter: SpokeWithListAdapter? = null

    private var mHomeAddressSameAsList: ArrayList<String>? = null
    private var mHomeAddressSameAsListAdapter: HomeAddressSameAsListAdapter? = null

    private var mSketchPlanList: ArrayList<String>? = null
    private var mSketchPlanListAdapter: SketchPlanListAdapter? = null

    private var mAppointmentTypeList: ArrayList<AppointmentTypeDataModel>? = null
    private var mAppointmentTypeListAdapter: AppointmentTypeListAdapter? = null

//    private var mAppointmentDateList: ArrayList<AppointmentDateModel>? = null
//    private var mAppointmentDateListAdapter: AppointmentDateListAdapter? = null

    private var mAppointmentLocationList: ArrayList<ShowRoomDataModel>? = null
    private var mAppointmentLocationListAdapter: AppointmentLocationListAdapter? = null

    private var mSelectedShowRoomPosition: Int = 0
    private var mSelectedSpokeWithPosition: Int = 0
    private var mSelectedValueForHomeAddressSameAsProjectPosition: Int = 0
    private var mSelectedProjectSketchPosition: Int = 0
    private var mSelectedAppointmentPosition: Int = 0
    //private var mSelectedAppointmentDatePosition: Int = 0
    private var mSelectedAppointmentLocPosition: Int = 0

    private var mCurrentlyWorkingWithValue: String = ""
    private var mSelectedCategoryDescribeBestValue: String = ""

    private var planSketchFilePath = ""
    private var planSketchFileUrl = ""

    private var mSelectedPickedDateTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_new_sign_in, container, false)
        setupToolbar()
        setupUI()
        setupClickListener()
        getShowRoom()
        return mView
    }

    private fun setupToolbar() {
//        (activity as HomeActivity).setToolbarTitle(activity?.resources?.getString(R.string.business)!!)
//        (activity as HomeActivity).setToolbarMenuIconVisibility(false)
//        (activity as HomeActivity).setToolbarBackIconVisibility(true)
//        (activity as HomeActivity).setToolbarAddIconVisibility(false)
//        (activity as HomeActivity).setToolbarInvoiceIconVisibility(false)
//        (activity as HomeActivity).setToolbarPartiesIconVisibility(false)
    }

    private fun setupUI() {
        mImageCaptureHelper = ImageSelectionHelper(requireActivity(), this)
        mAuth = FirebaseAuth.getInstance()

        mView?.et_otherCategory?.visibility = View.GONE
        mView?.tv_projectAddressLabel?.visibility = View.GONE
        mView?.et_projectAddress?.visibility = View.GONE
        mView?.btn_addSketchPlan?.visibility = View.GONE

        mView?.tv_appointmentAddressLabel?.visibility = View.GONE
        mView?.et_appointmentAddress?.visibility = View.GONE

        mShowRoomList = ArrayList<ShowRoomDataModel>()
        val showRoomItem = ShowRoomDataModel()
        showRoomItem.name = "Select Showroom"
        mShowRoomList?.add(showRoomItem)
        mShowRoomListAdapter = ShowRoomListAdapter(activity!!, mShowRoomList)
        mView?.spinner_showroomType?.adapter = mShowRoomListAdapter
        mView?.spinner_showroomType?.onItemSelectedListener = this

        mSpokeWithList = ArrayList<SpokeWithDataModel>()
        val spokeWith = SpokeWithDataModel()
        spokeWith.first_name = "Select"
        mSpokeWithList?.add(spokeWith)
        mSpokeWithListAdapter = SpokeWithListAdapter(activity!!, mSpokeWithList)
        mView?.spinner_spokeWith?.adapter = mSpokeWithListAdapter
        mView?.spinner_spokeWith?.onItemSelectedListener = this

        mHomeAddressSameAsList = ArrayList<String>()
        mHomeAddressSameAsList?.add("Yes, it’s the same")
        mHomeAddressSameAsList?.add("No, it’s different")
        mHomeAddressSameAsListAdapter =
            HomeAddressSameAsListAdapter(activity!!, mHomeAddressSameAsList)
        mView?.spinner_homeAddressSameAsProjectAddress?.adapter = mHomeAddressSameAsListAdapter
        mView?.spinner_homeAddressSameAsProjectAddress?.onItemSelectedListener = this

        mSketchPlanList = ArrayList<String>()
        mSketchPlanList?.add("Make Selection")
        mSketchPlanList?.add("Yes")
        mSketchPlanList?.add("No")
        mSketchPlanListAdapter = SketchPlanListAdapter(activity!!, mSketchPlanList)
        mView?.spinner_projectPlanSketch?.adapter = mSketchPlanListAdapter
        mView?.spinner_projectPlanSketch?.onItemSelectedListener = this

        mAppointmentTypeList = ArrayList<AppointmentTypeDataModel>()
        val appointmentType = AppointmentTypeDataModel()
        appointmentType.name = "Make Selection"
        mAppointmentTypeList?.add(appointmentType)
        mAppointmentTypeListAdapter = AppointmentTypeListAdapter(activity!!, mAppointmentTypeList)
        mView?.spinner_appointmentType?.adapter = mAppointmentTypeListAdapter
        mView?.spinner_appointmentType?.onItemSelectedListener = this

//        mAppointmentDateList = ArrayList<AppointmentDateModel>()
//        val appointmentDate = AppointmentDateModel()
//        appointmentDate.start_date_time = "Make Selection"
//        mAppointmentDateList?.add(appointmentDate)
//        mAppointmentDateListAdapter = AppointmentDateListAdapter(activity!!, mAppointmentDateList)
//        mView?.spinner_appointmentDate?.adapter = mAppointmentDateListAdapter
//        mView?.spinner_appointmentDate?.onItemSelectedListener = this

        mAppointmentLocationList = ArrayList<ShowRoomDataModel>()
        val appointmentLocation = ShowRoomDataModel()
        appointmentLocation.name = "Select Address"
        mAppointmentLocationList?.add(appointmentLocation)
        mAppointmentLocationListAdapter =
            AppointmentLocationListAdapter(activity!!, mAppointmentLocationList)
        mView?.spinner_appointmentLoc?.adapter = mAppointmentLocationListAdapter
        mView?.spinner_appointmentLoc?.onItemSelectedListener = this

        mView?.rg_selectCategory?.setOnCheckedChangeListener(object :
            RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                val radioBtn = group?.findViewById(checkedId) as RadioButton
                val radioBtnId = mView?.rg_selectCategory?.checkedRadioButtonId
                when (radioBtnId) {
                    R.id.radio_homeOwner -> {

                        mSelectedCategoryDescribeBestValue = "52"
                        mView?.et_otherCategory?.visibility = View.GONE
                    }
                    R.id.radio_realtor -> {
                        mSelectedCategoryDescribeBestValue = "50"
                        mView?.et_otherCategory?.visibility = View.GONE
                    }
                    R.id.radio_architect -> {
                        mSelectedCategoryDescribeBestValue = "51"
                        mView?.et_otherCategory?.visibility = View.GONE
                    }
                    R.id.radio_contractor -> {
                        mSelectedCategoryDescribeBestValue = "53"
                        mView?.et_otherCategory?.visibility = View.GONE
                    }
                    R.id.radio_interiorDesigner -> {
                        mSelectedCategoryDescribeBestValue = "49"
                        mView?.et_otherCategory?.visibility = View.GONE
                    }
                    R.id.radio_other -> {
                        mSelectedCategoryDescribeBestValue = "other"
                        mView?.et_otherCategory?.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun setupClickListener() {
        mView?.btn_addSketchPlan?.setOnClickListener(this)
        mView?.btn_submit?.setOnClickListener(this)

        mView?.lyt_showroomSpinner?.setOnClickListener(this)
        mView?.iv_showroomDropDown?.setOnClickListener(this)

        mView?.lyt_spokeWithSpinner?.setOnClickListener(this)
        mView?.iv_spokeWithDropDown?.setOnClickListener(this)

        mView?.lyt_projectAddress?.setOnClickListener(this)
        mView?.iv_projectAddressDropDown?.setOnClickListener(this)

        mView?.lyt_planSketch?.setOnClickListener(this)
        mView?.iv_planDropDown?.setOnClickListener(this)

        mView?.lyt_appointmentType?.setOnClickListener(this)
        mView?.iv_appointmentDropDown?.setOnClickListener(this)

        mView?.lyt_appointmentDate?.setOnClickListener(this)
        //mView?.iv_appointmentDateDropDown?.setOnClickListener(this)

        mView?.lyt_appointmentLoc?.setOnClickListener(this)
        mView?.iv_appointmentLoc?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_addSketchPlan -> {
                mImageCaptureHelper?.openImageSelectionDialog()
            }
            R.id.lyt_showroomSpinner -> {
                mView?.spinner_showroomType?.performClick()
            }
            R.id.iv_showroomDropDown -> {
                mView?.spinner_showroomType?.performClick()
            }
            R.id.lyt_spokeWithSpinner -> {
                mView?.spinner_spokeWith?.performClick()
            }
            R.id.iv_spokeWithDropDown -> {
                mView?.spinner_spokeWith?.performClick()
            }
            R.id.lyt_projectAddress -> {
                mView?.spinner_homeAddressSameAsProjectAddress?.performClick()
            }
            R.id.iv_projectAddressDropDown -> {
                mView?.spinner_homeAddressSameAsProjectAddress?.performClick()
            }
            R.id.lyt_planSketch -> {
                mView?.spinner_projectPlanSketch?.performClick()
            }
            R.id.iv_planDropDown -> {
                mView?.spinner_projectPlanSketch?.performClick()
            }
            R.id.lyt_appointmentType -> {
                mView?.spinner_appointmentType?.performClick()
            }
            R.id.iv_appointmentDropDown -> {
                mView?.spinner_appointmentType?.performClick()
            }
            R.id.lyt_appointmentDate -> {
                pickDateTime()
                //mView?.spinner_appointmentDate?.performClick()
            }
//            R.id.iv_appointmentDateDropDown -> {
//                mView?.spinner_appointmentDate?.performClick()
//            }
            R.id.lyt_appointmentLoc -> {
                mView?.spinner_appointmentLoc?.performClick()
            }
            R.id.iv_appointmentLoc -> {
                mView?.spinner_appointmentLoc?.performClick()
            }
            R.id.btn_submit -> {
                Utility.hideKeyboard(activity!!, mView?.btn_submit!!)
                if (NetworkUtils.isNetworkAvailable(activity!!)) {
                    if (isValidUI()) {
                        if (mSelectedProjectSketchPosition == 1) {
                            if (mImageFile != null) {
                                uploadImageToFirebase()
                            } else {
                                ToastMsgUtils.showSuccessMsg(
                                    mView?.lyt_parent!!,
                                    "Please add your sketch plan document"
                                )
                            }
                        } else {
                            initiateNewSignInProcess()
                        }
                    }
                } else {
                    ToastMsgUtils.showSuccessMsg(
                        mView?.lyt_parent!!,
                        getString(R.string.error_msg_network)
                    )
                }
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.id) {
            R.id.spinner_showroomType -> {
                mSelectedShowRoomPosition = position
            }
            R.id.spinner_spokeWith -> {
                mSelectedSpokeWithPosition = position
            }
            R.id.spinner_homeAddressSameAsProjectAddress -> {
                mSelectedValueForHomeAddressSameAsProjectPosition = position
                manageProjectAddressField()
            }
            R.id.spinner_projectPlanSketch -> {
                mSelectedProjectSketchPosition = position
                manageAddSketchPlanButtonVisibility()
            }
            R.id.spinner_appointmentType -> {
                mSelectedAppointmentPosition = position
            }
//            R.id.spinner_appointmentDate -> {
//                mSelectedAppointmentDatePosition = position
//            }
            R.id.spinner_appointmentLoc -> {
                mSelectedAppointmentLocPosition = position
                manageAppointmentAddressFieldVisibility()
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    private fun manageProjectAddressField() {
        when (mSelectedValueForHomeAddressSameAsProjectPosition) {
            0 -> {
                mView?.tv_projectAddressLabel?.visibility = View.GONE
                mView?.et_projectAddress?.visibility = View.GONE
            }
            1 -> {
                mView?.tv_projectAddressLabel?.visibility = View.VISIBLE
                mView?.et_projectAddress?.visibility = View.VISIBLE
            }
        }
    }

    private fun manageAddSketchPlanButtonVisibility() {
        when (mSelectedProjectSketchPosition) {
            0 -> {
                mView?.btn_addSketchPlan?.visibility = View.GONE
            }
            1 -> {
                mView?.btn_addSketchPlan?.visibility = View.VISIBLE
            }
            2 -> {
                mView?.btn_addSketchPlan?.visibility = View.GONE
            }
        }
    }

    private fun manageAppointmentAddressFieldVisibility() {
        val appointmentLocation = mAppointmentLocationList?.get(mSelectedAppointmentLocPosition)

        if (!TextUtils.isEmpty(appointmentLocation?.city)) {
            if (appointmentLocation?.city.equals("Enter new address")) {
                mView?.tv_appointmentAddressLabel?.visibility = View.VISIBLE
                mView?.et_appointmentAddress?.visibility = View.VISIBLE
            } else {
                mView?.tv_appointmentAddressLabel?.visibility = View.GONE
                mView?.et_appointmentAddress?.visibility = View.GONE
            }
        }
    }

    private fun isValidUI(): Boolean {
        var isValid: Boolean = false
        if (mSelectedShowRoomPosition == 0) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(mView?.lyt_parent!!, "Showroom is required")
        } else if (TextUtils.isEmpty(mView?.et_firstName?.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(mView?.lyt_parent!!, "First name is required")
        } else if (TextUtils.isEmpty(mView?.et_lastName?.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(mView?.lyt_parent!!, "Last name is required")
        } else if (mSelectedSpokeWithPosition == 0) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(mView?.lyt_parent!!, "Spoke with is required")
        } else if (TextUtils.isEmpty(mView?.et_emailAddress?.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(
                mView?.lyt_parent!!,
                getString(R.string.error_msg_email_required)
            )
        } else if (!Patterns.EMAIL_ADDRESS.matcher(mView?.et_emailAddress?.text.toString().trim())
                .matches()
        ) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(
                mView?.lyt_parent!!,
                getString(R.string.error_msg_not_a_valid_email)
            )
        } else if (TextUtils.isEmpty(mView?.et_aboutUs?.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(
                mView?.lyt_parent!!,
                "How did you hear about us? is required"
            )
        } else if (mSelectedAppointmentPosition != 0) {
            if (mSelectedAppointmentLocPosition == 0) {
                isValid = false
                ToastMsgUtils.showSuccessMsg(
                    mView?.lyt_parent!!,
                    "All the appointment fields are required"
                )
            } else if (mAppointmentLocationList?.get(mSelectedAppointmentLocPosition)?.city!!.equals(
                    "Enter new address"
                )
            ) {
                if (TextUtils.isEmpty(mView?.et_appointmentAddress?.text.toString().trim())) {
                    isValid = false
                    ToastMsgUtils.showSuccessMsg(
                        mView?.lyt_parent!!,
                        "All the appointment fields are required"
                    )
                } else {
                    isValid = true
                }
            } else {
                isValid = true
            }
        } else {
            isValid = true
        }
        return isValid
    }

    private fun getShowRoom() {
        if (NetworkUtils.isNetworkAvailable(activity!!)) {
            mView?.lyt_progressBar?.visibility = View.VISIBLE
            db.collection("showrooms")
                .get()
                .addOnSuccessListener { result ->
                    val showRooms = ArrayList<ShowRoomDataModel>()
                    Log.e("8*******", result.size().toString())
                    for (document in result) {
                        val showRoomData = ShowRoomDataModel()
                        val showRoomResult = document.data
                        if (showRoomResult.containsKey("id")) {
                            showRoomData.id = showRoomResult.get("id") as String
                        }
                        if (showRoomResult.containsKey("name") && showRoomResult.get("name") != null) {
                            showRoomData.name = showRoomResult.get("name") as String
                        }
                        if (showRoomResult.containsKey("lead_email") && showRoomResult.get("lead_email") != null) {
                            showRoomData.lead_email = showRoomResult.get("lead_email") as String
                        }
                        if (showRoomResult.containsKey("city") && showRoomResult.get("city") != null) {
                            showRoomData.city = showRoomResult.get("city") as String
                        }
                        if (showRoomResult.containsKey("client_email") && showRoomResult.get("client_email") != null) {
                            showRoomData.client_email = showRoomResult.get("client_email") as String
                        }
                        if (showRoomResult.containsKey("country") && showRoomResult.get("country") != null) {
                            showRoomData.country = showRoomResult.get("country") as String
                        }
                        if (showRoomResult.containsKey("project_email") && showRoomResult.get("project_email") != null) {
                            showRoomData.project_email =
                                showRoomResult.get("project_email") as String
                        }
                        if (showRoomResult.containsKey("signin_email") && showRoomResult.get("signin_email") != null) {
                            showRoomData.signin_email = showRoomResult.get("signin_email") as String
                        }
                        if (showRoomResult.containsKey("state") && showRoomResult.get("state") != null) {
                            showRoomData.state = showRoomResult.get("state") as String
                        }
                        if (showRoomResult.containsKey("street_address") && showRoomResult.get("street_address") != null) {
                            showRoomData.street_address =
                                showRoomResult.get("street_address") as String
                        }
                        if (showRoomResult.containsKey("zip") && showRoomResult.get("zip") != null) {
                            showRoomData.zip = showRoomResult.get("zip") as String
                        }
                        showRooms.add(showRoomData)
                        showRooms.sortWith(
                            compareBy(String.CASE_INSENSITIVE_ORDER, { it.name })
                        )
                    }

                    if (!showRooms.isNullOrEmpty()) {
                        mShowRoomList?.addAll(showRooms)
                        mShowRoomListAdapter?.notifyDataSetChanged()
                        mAppointmentLocationList?.addAll(showRooms)

                        //For Appointment Location
                        val showRoomData = ShowRoomDataModel()
                        showRoomData.name = "Enter new address"
                        showRoomData.city = "Enter new address"
                        mAppointmentLocationList?.add(showRoomData)
                        mAppointmentLocationListAdapter?.notifyDataSetChanged()
                    }
                    getSpokeWithData()
                }
                .addOnFailureListener { exception ->
                    mView?.lyt_progressBar?.visibility = View.GONE
                }
        } else {
            ToastMsgUtils.showSuccessMsg(fragment_container, getString(R.string.error_msg_network))
        }
    }

    private fun getSpokeWithData() {
        if (NetworkUtils.isNetworkAvailable(activity!!)) {
            db.collection("users")
                .get()
                .addOnSuccessListener { result ->
                    val spokeWithList = ArrayList<SpokeWithDataModel>()
                    for (document in result) {
                        val spokeWithData = SpokeWithDataModel()
                        val spokeWithResult = document.data
                        if (spokeWithResult.containsKey("id")) {
                            spokeWithData.id = spokeWithResult.get("id") as String
                        }
                        if (spokeWithResult.containsKey("first_name") && spokeWithResult.get("first_name") != null) {
                            spokeWithData.first_name = spokeWithResult.get("first_name") as String
                        }
                        if (spokeWithResult.containsKey("last_name") && spokeWithResult.get("last_name") != null) {
                            spokeWithData.last_name = spokeWithResult.get("last_name") as String
                        }
                        if (spokeWithResult.containsKey("email") && spokeWithResult.get("email") != null) {
                            spokeWithData.email = spokeWithResult.get("email") as String
                        }
                        if (spokeWithResult.containsKey("author_user_id") && spokeWithResult.get("author_user_id") != null) {
                            spokeWithData.author_user_id =
                                spokeWithResult.get("author_user_id") as String
                        }
                        if (spokeWithResult.containsKey("date_created") && spokeWithResult.get("date_created") != null) {
                            spokeWithData.date_created = spokeWithResult.get("date_created") as Long
                        }
                        spokeWithList.add(spokeWithData)
                        spokeWithList.sortWith(
                            compareBy(String.CASE_INSENSITIVE_ORDER, { it.first_name })
                        )
                    }

                    if (!spokeWithList.isNullOrEmpty()) {
                        mSpokeWithList?.addAll(spokeWithList)
                        mSpokeWithListAdapter?.notifyDataSetChanged()
                    }
                    getAppointmentType()
                }
                .addOnFailureListener { exception ->
                    mView?.lyt_progressBar?.visibility = View.GONE
                }
        } else {
            ToastMsgUtils.showSuccessMsg(mView?.lyt_parent!!, getString(R.string.error_msg_network))
        }
    }

    private fun getAppointmentType() {
        if (NetworkUtils.isNetworkAvailable(activity!!)) {
            db.collection("calender_event_types")
                .get()
                .addOnSuccessListener { result ->
                    val appointmentTypeList = ArrayList<AppointmentTypeDataModel>()
                    for (document in result) {
                        val appointmentTypeData = AppointmentTypeDataModel()
                        val item = document.data
                        if (item.containsKey("id")) {
                            appointmentTypeData.id = item.get("id") as String
                        }
                        if (item.containsKey("name") && item.get("name") != null) {
                            appointmentTypeData.name = item.get("name") as String
                        }
                        if (item.containsKey("uid") && item.get("uid") != null) {
                            appointmentTypeData.uid = item.get("uid") as String
                        }
                        appointmentTypeList.add(appointmentTypeData)
                        appointmentTypeList.sortWith(
                            compareBy(String.CASE_INSENSITIVE_ORDER, { it.name })
                        )
                    }

                    if (!appointmentTypeList.isNullOrEmpty()) {
                        mAppointmentTypeList?.addAll(appointmentTypeList)
                        mAppointmentTypeListAdapter?.notifyDataSetChanged()
                    }
                    mView?.lyt_progressBar?.visibility = View.GONE
                    //getAppointmentDate()
                }
                .addOnFailureListener { exception ->
                    mView?.lyt_progressBar?.visibility = View.GONE
                }
        } else {
            ToastMsgUtils.showSuccessMsg(mView?.lyt_parent!!, getString(R.string.error_msg_network))
        }
    }

    private fun getAppointmentDate() {
        if (NetworkUtils.isNetworkAvailable(activity!!)) {
            db.collection("calendar_events")
                .get()
                .addOnSuccessListener { result ->
                    mView?.lyt_progressBar?.visibility = View.GONE
                    val appointmentDateList = ArrayList<AppointmentDateModel>()
                    for (document in result) {
                        val appointmentDateData = AppointmentDateModel()
                        val item = document.data
                        if (item.containsKey("id")) {
                            appointmentDateData.id = item.get("id") as String
                        }
                        if (item.containsKey("start_date_time") && item.get("start_date_time") != null) {
                            appointmentDateData.start_date_time =
                                item.get("start_date_time") as String
                        }
                        appointmentDateList.add(appointmentDateData)
                    }

                    if (!appointmentDateList.isNullOrEmpty()) {
                        //mAppointmentDateList?.addAll(appointmentDateList)
                        //mAppointmentDateListAdapter?.notifyDataSetChanged()
                    }
                }
                .addOnFailureListener { exception ->
                    mView?.lyt_progressBar?.visibility = View.GONE
                }
        } else {
            ToastMsgUtils.showSuccessMsg(mView?.lyt_parent!!, getString(R.string.error_msg_network))
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mImageCaptureHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mImageCaptureHelper?.onActivityResult(requestCode, resultCode, data)
    }

    override fun capturedImage(uri: Uri?, imageFile: File?) {
        if (imageFile != null) {
            mImageFile = imageFile
            mView?.btn_addSketchPlan?.background = ContextCompat.getDrawable(requireActivity(), R.drawable.bg_solid_green)
            mView?.tv_addSketchPlan?.text = "File Added Successfully"
            mView?.iv_success?.visibility = View.VISIBLE
        }
    }

    private fun uploadImageToFirebase() {
        mView?.lyt_progressBar?.visibility = View.VISIBLE
        val id = Prefs.getString(AppConstant.USER_ID)
        val fileUri = Uri.fromFile(mImageFile)
        val storageReference = FirebaseStorage.getInstance().reference

        val filepath = storageReference
            .child("signins/")
            .child(id + "/")
            .child(mImageFile?.name!!)

        filepath.putFile(fileUri)
            .addOnSuccessListener(
                OnSuccessListener<UploadTask.TaskSnapshot?> {
                    filepath.getDownloadUrl().addOnSuccessListener(
                        OnSuccessListener<Uri> { uri ->
                            mView?.lyt_progressBar?.visibility = View.GONE
                            val path = uri.path
                            planSketchFilePath = path!!
                            planSketchFileUrl = uri.toString()
                            initiateNewSignInProcess()
                        }
                    )
                }
            )
            .addOnFailureListener { exception ->
                mView?.lyt_progressBar?.visibility = View.GONE
                ToastMsgUtils.showErrorMsg(mView?.lyt_parent!!, exception.message!!)
            }
    }

    private fun initiateNewSignInProcess() {
        mView?.lyt_progressBar?.visibility = View.VISIBLE
        val mDatabaseref = db.collection("signins").document()
        val documentId = mDatabaseref.id
        val dateTime = Calendar.getInstance().timeInMillis
        val newSignInModel = NewSignInModel(
            documentId,
            Prefs.getString(AppConstant.USER_ID),
            mShowRoomList?.get(mSelectedShowRoomPosition)?.id!!,
            mView?.et_firstName?.text.toString().trim(),
            mView?.et_lastName?.text.toString().trim(),
            getSelectedSpokeWith(),
            mView?.et_phoneNumber?.text.toString().trim(),
            mView?.et_mobileNumber?.text.toString().trim(),
            mView?.et_emailAddress?.text.toString().trim(),
            getCurrentlyWorkingWithValue(),
            mSelectedCategoryDescribeBestValue,
            mView?.et_otherCategory?.text.toString().trim(),
            mView?.et_homeAddress?.text.toString().trim(),
            getProjectAddress(),
            mView?.et_projectBudget?.text.toString().trim(),
            mView?.et_appliance?.text.toString().trim(),
            mView?.et_styleFinishes?.text.toString().trim(),
            mSketchPlanList?.get(mSelectedProjectSketchPosition)!!,
            planSketchFilePath,
            planSketchFileUrl,
            getSelectedAppointmentType(),
            getSelectedAppointmentDate(),
            mView?.et_appointmentAddress?.text.toString().trim(),
            getSelectedAppointmentLocation(),
            mView?.et_purposeOfVisit?.text.toString().trim(),
            mView?.et_aboutUs?.text.toString().trim(),
            mView?.et_notes?.text.toString().trim(),
            dateTime,
            dateTime
        )
        db.collection("signins")
            .document(documentId)
            .set(newSignInModel)
            .addOnSuccessListener { result ->
                mView?.lyt_progressBar?.visibility = View.GONE
                resetAllField()
                ToastMsgUtils.showSuccessMsg(mView?.lyt_parent!!, "Sign-In successfully created")
            }
            .addOnFailureListener { exception ->
                mView?.lyt_progressBar?.visibility = View.GONE
                Log.w("TAG", "Error getting documents.", exception)
            }
    }

    private fun getSelectedAppointmentType(): String {
        var appointmentType = ""
        if (mSelectedAppointmentPosition == 0) {
            appointmentType = ""
        } else {
            appointmentType = mAppointmentTypeList?.get(mSelectedAppointmentPosition)?.id!!
        }
        return appointmentType
    }

    private fun getSelectedAppointmentDate(): Long {
        var appointmentDate: Long = 0
        //appointmentDate = DateUtils.getServerDate(mSelectedPickedDateTime, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").toString()
        appointmentDate = mSelectedPickedDateTime
        return appointmentDate
    }

    private fun getSelectedAppointmentLocation(): String {
        var appointmentLocation = ""
        if (mSelectedAppointmentLocPosition == 0) {
            appointmentLocation = ""
        } else {
            appointmentLocation =
                mAppointmentLocationList?.get(mSelectedAppointmentLocPosition)?.id!!
        }
        return appointmentLocation
    }

    private fun getCurrentlyWorkingWithValue(): ArrayList<String> {
        val selectedWorkingWithIds = ArrayList<String>()
        if (mView?.cb_architect?.isChecked!!) {
            selectedWorkingWithIds.add("54")
        }
        if (mView?.cb_generalContractor?.isChecked!!) {
            selectedWorkingWithIds.add("56")
        }
        if (mView?.cb_interiorDesigner?.isChecked!!) {
            selectedWorkingWithIds.add("55")
        }
        return selectedWorkingWithIds
    }

    private fun getSelectedSpokeWith(): String {
        val spokeWithName = mSpokeWithList?.get(mSelectedSpokeWithPosition)?.id!!
        return spokeWithName
    }

    private fun getProjectAddress(): String {
        var projectAddress = ""
        when (mSelectedValueForHomeAddressSameAsProjectPosition) {
            0 -> {
                if (!TextUtils.isEmpty(mView?.et_homeAddress?.text.toString().trim())) {
                    projectAddress = mView?.et_homeAddress?.text.toString().trim()
                } else {
                    projectAddress = ""
                }
            }
            1 -> {
                projectAddress = mView?.et_projectAddress?.text.toString().trim()
            }
        }
        return projectAddress
    }

    private fun resetAllField() {
        mView?.spinner_showroomType?.setSelection(0)
        mView?.spinner_spokeWith?.setSelection(0)

        mView?.et_firstName?.setText("")
        mView?.et_lastName?.setText("")
        mView?.et_mobileNumber?.setText("")
        mView?.et_phoneNumber?.setText("")
        mView?.et_emailAddress?.setText("")

        if (mView?.cb_architect?.isChecked!!) {
            mView?.cb_architect?.isChecked = false
        }

        if (mView?.cb_generalContractor?.isChecked!!) {
            mView?.cb_generalContractor?.isChecked = false
        }

        if (mView?.cb_interiorDesigner?.isChecked!!) {
            mView?.cb_interiorDesigner?.isChecked = false
        }

        val radioBtnId = mView?.rg_selectCategory?.checkedRadioButtonId
        when (radioBtnId) {
            R.id.radio_homeOwner -> {
                mView?.radio_homeOwner?.isChecked = false
            }
            R.id.radio_realtor -> {
                mView?.radio_realtor?.isChecked = false
            }
            R.id.radio_architect -> {
                mView?.radio_architect?.isChecked = false
            }
            R.id.radio_contractor -> {
                mView?.radio_contractor?.isChecked = false
            }
            R.id.radio_interiorDesigner -> {
                mView?.radio_interiorDesigner?.isChecked = false
            }
            R.id.radio_other -> {
                mView?.radio_other?.isChecked = false
            }
        }

        mView?.et_otherCategory?.setText("")
        mView?.et_otherCategory?.visibility = View.GONE
        mView?.et_homeAddress?.setText("")
        mView?.et_projectAddress?.setText("")
        mView?.et_projectBudget?.setText("")
        mView?.et_appliance?.setText("")
        mView?.et_styleFinishes?.setText("")
        mView?.et_appointmentAddress?.setText("")

        mView?.et_purposeOfVisit?.setText("")
        mView?.et_aboutUs?.setText("")
        mView?.et_notes?.setText("")

        mView?.spinner_homeAddressSameAsProjectAddress?.setSelection(0)
        mView?.spinner_projectPlanSketch?.setSelection(0)

        mImageFile = null
        mView?.btn_addSketchPlan?.background = ContextCompat.getDrawable(requireActivity(), R.drawable.bg_solid_blue_rounded_corner)
        mView?.tv_addSketchPlan?.text = resources.getString(R.string.add_sketch_plan)
        mView?.iv_success?.visibility = View.GONE

        mView?.spinner_appointmentType?.setSelection(0)
        //mView?.spinner_appointmentDate?.setSelection(0)
        mView?.tv_selectedDateTime?.text = ""
        mView?.spinner_appointmentLoc?.setSelection(0)
    }

    private fun pickDateTime() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        val startHour = currentDateTime.get(Calendar.HOUR_OF_DAY)
        val startMinute = currentDateTime.get(Calendar.MINUTE)

        DatePickerDialog(requireContext(), DatePickerDialog.OnDateSetListener { _, year, month, day ->
            TimePickerDialog(requireContext(), TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day, hour, minute)
                mSelectedPickedDateTime = pickedDateTime.timeInMillis
                val selectedDateTime = DateUtils.getFormattedDateTime(pickedDateTime.timeInMillis, "MMM d, yyyy 'at'  hh:mm  a").toString()
                mView?.tv_selectedDateTime?.text = selectedDateTime
                //doSomethingWith(pickedDateTime)
            }, startHour, startMinute, false).show()
        }, startYear, startMonth, startDay).show()
    }
}