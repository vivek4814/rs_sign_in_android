package com.remodelingdata.rssignin.ui.activity

import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.remodelingdata.rssignin.R
import com.remodelingdata.rssignin.utils.NetworkUtils
import com.remodelingdata.rssignin.utils.ToastMsgUtils
import com.remodelingdata.rssignin.utils.Utility
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.toolbar_secondry.*


class ForgotPasswordActivity : AppCompatActivity(), View.OnClickListener {

    private var toolbar: Toolbar? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        initialiseUI()
        setupClickListener()
    }

    private fun initialiseUI()
    {
        mAuth = FirebaseAuth.getInstance()
    }

    private fun setupClickListener() {
        btn_sendPassword.setOnClickListener(this)
        iv_toolbarBack.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }

            R.id.btn_sendPassword -> {
                if (NetworkUtils.isNetworkAvailable(this@ForgotPasswordActivity)) {
                    if (isValidate()) {
                        Utility.hideKeyboard(this, btn_sendPassword)
                        sendPasswordResetEmail()
                    }
                } else {
                    ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_network))
                }
            }
        }
    }

    private fun isValidate(): Boolean {
        var isValid = false
        if (TextUtils.isEmpty(et_email.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_email_required))
        } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.text.toString().trim()).matches()) {
            isValid = false
            ToastMsgUtils.showErrorMsg(lyt_parent, getString(R.string.error_msg_not_a_valid_email))
        } else {
            isValid = true
        }
        return isValid
    }

    private fun sendPasswordResetEmail() {
        lyt_progressBar.visibility = View.VISIBLE
        mAuth?.sendPasswordResetEmail(et_email.text.toString().trim())!!
            .addOnCompleteListener(OnCompleteListener { task ->
                if (task.isSuccessful) {
                    lyt_progressBar.visibility = View.GONE
                    showAlertDialog()
                } else {
                    lyt_progressBar.visibility = View.GONE
                    ToastMsgUtils.showErrorMsg(lyt_parent, "Failed to send reset email!")
                }
            })
    }

    private fun showAlertDialog() {
        val alertDialog = AlertDialog.Builder(this)

        alertDialog.setMessage("Check your email for further instructions")
            .setCancelable(false)
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                finish()
            })
        // create dialog box
        val alert = alertDialog.create()
        // set title for alert dialog box
        alert.setTitle("Alert")
        // show alert dialog
        alert.show()
    }
}