package com.remodelingdata.rssignin.ui.activity

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.remodelingdata.rssignin.R
import com.remodelingdata.rssignin.ui.fragment.NewSignInFragment
import com.ve.countertop.preferences.Prefs
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.toolbar_main.*


class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private var mAuthListener: FirebaseAuth.AuthStateListener? = null
    private var mCurrentUserUid: String? = null
    private var mAuth: FirebaseAuth? = null
    val db = Firebase.firestore

    private var navView: NavigationView? = null
    private var toolbar: Toolbar? = null
    private var mDrawerLayout: DrawerLayout? = null
    private var mDrawerToggle: ActionBarDrawerToggle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        setAuthInstance()
        setAuthListener()
        initialiseNavigationBar()
        setupClickListener()
        setDisplayFragment(1)
    }

    private fun initialiseNavigationBar() {
        mDrawerLayout = findViewById(R.id.drawer_layout)

        navView = findViewById(R.id.nav_view)

        val headerLayout = navView?.getHeaderView(0)

        mDrawerToggle = ActionBarDrawerToggle(
            this,
            mDrawerLayout,
            toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )

        mDrawerLayout?.addDrawerListener(mDrawerToggle!!)
        navView?.setNavigationItemSelectedListener(this)
    }

    fun setAuthInstance() {
        mAuth = FirebaseAuth.getInstance()
    }

    fun setAuthListener() {
        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                setUserData(user)
            } else {
                // User is signed out
                goToLogin()
            }
        }
    }

    fun setUserData(user: FirebaseUser) {
        mCurrentUserUid = user.uid
    }

    fun goToLogin() {
        val loginIntent = Intent(this@HomeActivity, LoginActivity::class.java)
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(loginIntent)
        finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onStart() {
        super.onStart()
        mAuth?.addAuthStateListener(mAuthListener!!)
    }

    private fun logout() {
        mAuth?.signOut()
    }

    private fun setupClickListener() {
        iv_hamburgerMenu.setOnClickListener(this)
//        lyt_logout.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_toolbarBack -> {
                finish()
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }

            R.id.iv_hamburgerMenu -> {
                openDrawerLayout()
            }
//            R.id.lyt_logout -> {
//                mDrawerLayout?.closeDrawer(GravityCompat.START)
//                showLogOutConfirmationDialog()
//            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_logout -> {
                mDrawerLayout?.closeDrawer(GravityCompat.START)
                showLogOutConfirmationDialog()
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun setDisplayFragment(fragmentType: Int) {
        var mFragment: Fragment? = null
        var fragmentName: String = ""
        when (fragmentType) {
            1 -> {
                mFragment = NewSignInFragment()
                fragmentName = "NewSignInFragment"
                replaceFragment(mFragment, fragmentName)
            }
        }
    }

    fun replaceFragment(mFragment: Fragment, fragmentName: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, mFragment)
            .addToBackStack(fragmentName)
            .commit()
    }

    fun addFragment(mFragment: Fragment, fragmentName: String) {
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container, mFragment)
            .addToBackStack(fragmentName)
            .commit()
    }

    fun clearFragmentBackStack(name: String) {
//        supportFragmentManager.popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE)
//        val fragment = HomeFragment()
//        replaceFragment(fragment, "HomeFragment")
//        navView?.setCheckedItem(R.id.nav_home)
    }

    private fun removeFragmentFromBackStack() {
        if (supportFragmentManager.findFragmentById(R.id.fragment_container) is NewSignInFragment) {
            supportFragmentManager.popBackStackImmediate()
            finish()
        }
    }

    override fun onBackPressed() {
        removeFragmentFromBackStack()
    }

    fun openDrawerLayout() {
        drawer_layout.openDrawer(GravityCompat.START)
    }

    fun closeDrawerLayout() {
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    fun showLogOutConfirmationDialog() {
        val alertDialog = AlertDialog.Builder(this)

        alertDialog.setMessage("Are you sure you want to logout?")
            .setCancelable(false)
            .setPositiveButton("YES", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
                logout()
                Prefs.clear()
            })
            .setNegativeButton("NO",
                DialogInterface.OnClickListener { dialog, id -> //  Action for 'NO' Button
                    dialog.cancel()
                })
        // create dialog box
        val alert = alertDialog.create()
        // set title for alert dialog box
        alert.setTitle("Alert")
        // show alert dialog
        alert.show()
    }
}