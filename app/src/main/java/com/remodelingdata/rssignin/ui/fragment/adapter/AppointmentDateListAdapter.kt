package com.remodelingdata.rssignin.ui.fragment.adapter

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.remodelingdata.rssignin.R
import com.remodelingdata.rssignin.model.AppointmentDateModel
import com.remodelingdata.rssignin.utils.DateUtils

class AppointmentDateListAdapter (internal var context: Context, internal var showroomList: ArrayList<AppointmentDateModel>?) :
    ArrayAdapter<AppointmentDateModel>(context, 0, showroomList!!) {


    override fun getCount(): Int {
        return  showroomList?.size!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): AppointmentDateModel? {
        return showroomList?.get(position)
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var itemView = convertView
        if (itemView == null)
        {
            itemView = LayoutInflater.from(context).inflate(R.layout.text_layout,parent, false)
        }

        val label = itemView?.findViewById(R.id.text) as TextView
        if (position == 0)
        {
            label.setTextColor(context.resources.getColor(R.color.colorHint))
        }
        else
        {
            label.setTextColor(context.resources.getColor(R.color.colorBlack))
        }
        if (!TextUtils.isEmpty(showroomList?.get(position)?.start_date_time))
        {
            //val date = DateUtils.getFormattedDateTime(showroomList?.get(position)?.start_date_time!!, "MMM d, yyyy 'at'  hh:mm  a")
            //label.text = date
        }
        return itemView
    }

    override fun getDropDownView(position: Int, convertView: View?,
                                 parent: ViewGroup): View {
        var dropDownItemView = convertView
        if (dropDownItemView == null)
        {
            dropDownItemView = LayoutInflater.from(context).inflate(R.layout.item_dropdown_view, parent, false)
        }
        val label = dropDownItemView?.findViewById(R.id.tv_text) as TextView
        if (!TextUtils.isEmpty(showroomList?.get(position)?.start_date_time))
        {
            //val date = DateUtils.getFormattedDateTime(showroomList?.get(position)?.start_date_time!!, "EEE  MMM d,  hh:mm  a")
            //label.text = date
        }
        return label
    }
}