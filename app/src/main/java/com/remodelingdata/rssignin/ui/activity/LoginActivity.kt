package com.remodelingdata.rssignin.ui.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.remodelingdata.rssignin.R
import com.remodelingdata.rssignin.constants.AppConstant
import com.remodelingdata.rssignin.utils.NetworkUtils
import com.remodelingdata.rssignin.utils.ToastMsgUtils
import com.remodelingdata.rssignin.utils.Utility
import com.ve.countertop.preferences.Prefs
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupUI()
        setupClickListener()
    }

    private fun setupUI() {
        mAuth = FirebaseAuth.getInstance()
    }

    private fun setupClickListener() {
        btn_login.setOnClickListener(this)
        tv_forgotPasswordLabel.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_login -> {
                if (NetworkUtils.isNetworkAvailable(this@LoginActivity)) {
                    if (isValidUI()) {
                        Utility.hideKeyboard(this@LoginActivity, btn_login)
                        initiateLoginProcess()
                    }
                } else {
                    ToastMsgUtils.showSuccessMsg(
                        lyt_parent!!,
                        getString(R.string.error_msg_network)
                    )
                }
            }
            R.id.tv_forgotPasswordLabel -> {
                Utility.hideKeyboard(this@LoginActivity, tv_forgotPasswordLabel)
                val signUpIntent = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
                startActivity(signUpIntent)
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        }
    }

    private fun isValidUI(): Boolean {
        var isValid: Boolean = false
        if (TextUtils.isEmpty(et_email.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(lyt_parent!!, getString(R.string.error_msg_email_required))
        } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.text.toString().trim()).matches()) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(lyt_parent!!, getString(R.string.error_msg_not_a_valid_email))
        } else if (TextUtils.isEmpty(et_password.text.toString().trim())) {
            isValid = false
            ToastMsgUtils.showSuccessMsg(lyt_parent!!, getString(R.string.error_msg_password_required))
        } else {
            isValid = true
        }
        return isValid
    }

    private fun initiateLoginProcess() {
        lyt_progressBar.visibility = View.VISIBLE
        mAuth?.signInWithEmailAndPassword(
            et_email.text.toString().trim(),
            et_password.text.toString().trim()
        )
            ?.addOnCompleteListener(
                this
            ) { task ->
                if (task.isSuccessful) {
                    lyt_progressBar.visibility = View.GONE
                    val user = task.getResult()?.getUser()!!
                    goToMainActivity(user)
                } else {
                    lyt_progressBar.visibility = View.GONE
                    ToastMsgUtils.showErrorMsg(lyt_parent, "" + task.exception?.message)
                }
            }
    }

    private fun goToMainActivity(user: FirebaseUser) {
        Prefs.putString(AppConstant.USER_ID, user.uid)
        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}
