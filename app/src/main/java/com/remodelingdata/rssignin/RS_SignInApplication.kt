package com.remodelingdata.rssignin

import android.app.Application
import android.content.ContextWrapper
import com.ve.countertop.preferences.Prefs

class RS_SignInApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        //FirebaseApp.initializeApp(this)
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }
}