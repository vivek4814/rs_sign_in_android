package com.remodelingdata.rssignin.model

data class AppointmentTypeDataModel (
    var id: String = "",
    var name: String = "",
    var uid: String = ""
)