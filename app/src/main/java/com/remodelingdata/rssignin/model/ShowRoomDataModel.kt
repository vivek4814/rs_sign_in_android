package com.remodelingdata.rssignin.model

data class ShowRoomDataModel (
    var id: String = "",
    var name: String = "",
    var lead_email: String = "",
    var city: String = "",
    var client_email: String = "",
    var country: String = "",
    var project_email: String = "",
    var signin_email: String = "",
    var state: String = "",
    var street_address: String = "",
    var zip: String = ""
)