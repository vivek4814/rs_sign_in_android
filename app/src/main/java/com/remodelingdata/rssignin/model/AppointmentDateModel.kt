package com.remodelingdata.rssignin.model

data class AppointmentDateModel (
    var id: String = "",
    var start_date_time: String = ""
)