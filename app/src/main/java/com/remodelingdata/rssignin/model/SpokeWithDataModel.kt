package com.remodelingdata.rssignin.model

data class SpokeWithDataModel (
    var id: String = "",
    var email: String = "",
    var first_name: String = "",
    var last_name: String = "",
    var author_user_id: String = "",
    var date_created: Long = 0
)